<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/admin', function () {
//     return view('login');
// });

Route::get('/admin','BackendController@index');
Route::post('/login','BackendController@login');

Route::get('/admin/product','ProductController@product');
Route::get('/admin/product/add','ProductController@add');
Route::get('/admin/product-type','ProductController@category');
Route::post('/admin/product-type/save','ProductController@save');
Route::post('/admin/product-type/delete','ProductController@delete');
Route::post('/admin/product-type/view','ProductController@view');
Route::post('/admin/product/upload','ProductController@store_product');

Route::get('/admin/typepay','PayController@typepay');
Route::get('/admin/pay','PayController@pay');
Route::post('/admin/pay/save','PayController@save');
Route::post('/admin/pay/delete','PayController@delete');

Route::get('/admin/dashboard', function () {
    return view('backend.dashboard');
});

