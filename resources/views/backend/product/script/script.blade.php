<script>
    let ma_cate = $('#manage_categories')
    let ma_edit_cate = $('.btn_open_update_modal')
    let ma_delete_cate = $('.btn_delete')
    let ma_view_cate = $('.btn_view')


    ma_cate.click(function(){
        $('#title_modal').html('เพิ่มประเภทสินค้า')
        $('#cate').val('')
        $('#hdid_edit').val('')
        $('#status').prop('checked', false);
        $('#modal_manage_cate').modal('show')
    })

    ma_edit_cate.click(function(){

        var name = $(this).attr('data-name') //attr = attribute
        var id = $(this).attr('data-id')
        var status = $(this).attr('data-status')

        $('#title_modal').html('แก้ไขประเภทสินค้า')
        $('#modal_manage_cate').modal('show')
        $('#cate').val(name)
        $('#hdid_edit').val(id)

        if(status == 0) {
            $('#status').prop('checked', true);
            $('#status').val('')

        }else{
            $('#status').prop('checked', false);
            $('#status').val('1')
        }

    })

        ma_delete_cate.click(function(){

        let id = $(this).attr('data-id')
        $('#hdid_delete').val(id)
        $('#modal_delete_cate').modal('show')


    })

        ma_view_cate.click(function(){

        var id = $(this).attr('data-id')
        var name = $(this).attr('data-name')
        var status = $(this).attr('data-status')
        var create = $(this).attr('data-create')
        var update = $(this).attr('data-update')

        if(status == 0) {
            $('#view_status').val('ปิดการใช้งาน')

        }else{
            $('#view_status').val('เปิดการใช้งาน')
        }

        $('#hdid_view').val(id)
        $('#name').val(name)
        // $('#view_status').val(status)
        $('#create_date').val(create)
        $('#update_date').val(update)
        $('#modal_view_cate').modal('show')


    })

</script>
