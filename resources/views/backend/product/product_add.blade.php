@extends('Layouts.backend')
@section('contents')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>เพิ่มสินค้า</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">หน้าหลัก</a></li>
              <li class="breadcrumb-item active">สินค้า</li>
              <li class="breadcrumb-item active">เพิ่มสินค้า</li>
            </ol>
          </div>
        </div>
        <div class="row">
            <div class="col-sm-10"></div>
            <div class="col-sm-2">
                <a id="manage_categories" class="btn btn-block btn-danger" href="{{url('/admin/product/')}}">ย้อนกลับ</a>
            </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<form action="{{url('/admin/product/upload')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">รายละเอียด</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="inputName">ชื่อสินค้า</label>
                <input type="text" id="inputName" name="prd_name" class="form-control" value="">
              </div>
              <div class="form-group">
                <label for="inputStatus">ประเภทสินค้า</label>
                <select class="form-control custom-select" name="type_id">
                    @foreach ($data as $datas)
                        <option value="{{$datas->id}}">{{$datas->name}}</option>
                    @endforeach

                </select>
              </div>
              <div class="form-group">
                <label for="inputDescription">รายละเอียดสินค้า</label>
                <textarea id="inputDescription" class="form-control" rows="4" name="description"></textarea>
              </div>
              <div class="form-group">
                <label for="inputStatus">สถานะ</label>
                <select class="form-control custom-select" name="status">
                  <option value="1">เปิดการใช้งาน</option>
                  <option value="0" >ปิดการใช้งาน</option>
                </select>
              </div>
              <div class="form-group">
                <label for="inputClientCompany">จำนวนที่มีอยู่ใน Stock</label>
                <input type="text" id="inputClientCompany" name="stock" class="form-control" value="">
              </div>
              <div class="form-group">
                <label for="image">รูปภาพ</label>
                <input type="file" id="image" name="image" class="form-control" value="">
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <input type="submit" value="บันทึกสินค้า" class="btn btn-success float-right">
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</form>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


@endsection
