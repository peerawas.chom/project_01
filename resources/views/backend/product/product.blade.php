@extends('Layouts.backend')
@section('contents')
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>จัดการประเภทสินค้า</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">หน้าหลัก</a></li>
              <li class="breadcrumb-item active">จัดการประเภทสินค้า</li>
            </ol>
          </div>
        </div>
        <div class="row">
            <div class="col-sm-10"></div>
            <div class="col-sm-2">
                <a id="manage_categories" class="btn btn-block btn-success" href="{{url('/admin/product/add')}}">เพิ่ม</a>
            </div>
        </div>

      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">จัดการประเภทสินค้า</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 1%">
                          #
                      </th>
                      <th style="width: 20%">
                          รูปภาพ
                      </th>
                      </th>
                      <th>
                          ชื่อสินค้า
                      </th>
                      <th>
                          จำนวนสินค้าใน Stock
                      </th>
                      <th style="width: 8%" class="text-center">
                          สถานะ
                      </th>
                      <th style="width: 20%">
                      </th>
                  </tr>
              </thead>
              <tbody>
                  <?php
                    $count=0;
                    ?>
                    @foreach ($pic as $pics)


                  <tr>
                      <td>
                        {{$count+=1}}
                      </td>
                      <td>
                          <img src="{{env('APP_PUBLIC')}}storage/product/{{$pics->image}}" width="200px" height="200px">
                      </td>
                      <td>
                        {{$pics->prd_name}}
                    </td>
                      <td class="project_progress">
                          <div class="progress progress-sm">
                              <div class="progress-bar bg-green" role="progressbar" aria-volumenow="75" aria-volumemin="0" aria-volumemax="100" style="width: 75%">
                              </div>
                          </div>
                          <small>
                            จำนวน {{$pics->stock}} ชิ้น
                          </small>
                      </td>
                      <td class="project-state">
                          <span class="badge badge-success">เปิดการใช้งาน</span>
                      </td>
                      <td class="project-actions text-right">
                          <a class="btn btn-primary btn-sm btn_view" href="#">
                              <i class="fas fa-folder">
                              </i>
                              View
                          </a>
                              <a class="btn btn-info btn-sm btn_open_update_modal" href="#">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a class="btn btn-danger btn-sm btn_delete" href="#">
                              <i class="fas fa-trash">
                              </i>
                              Delete
                          </a>
                      </td>
                  </tr>
                  @endforeach
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->


    </section>
    <!-- /.content -->
  </div>
  @include('backend.product.modal.manage_cate')
  @include('backend.product.modal.delete_cate')
  @include('backend.product.modal.view_cate')
  @include('backend.product.script.script')

  <!-- /.content-wrapper -->
@endsection
