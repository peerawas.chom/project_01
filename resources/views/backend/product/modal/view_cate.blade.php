<!-- Modal -->
<div class="modal fade" id="modal_view_cate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
      <form action="{{url('/admin/product-type/view')}}" method="POST">
        <div class="modal-header">
          <h5 class="modal-title" >รายการสินค้า</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
                <input type="hidden" name="hdid_view" id="hdid_view">
                @csrf
                <div class="form-group">
                <label for="cate">ชื่อประเภทสินค้า</label>
                <input type="txt" name="name" class="form-control" id="name"  value="" disabled>
                </div>
                <div class="form-group">
                <label for="status">สถานะ</label>
                <input type="txt" name="view_status" class="form-control" id="view_status" value="" disabled>
                </div>
                <div class="form-group">
                <label for="create_date">วันที่เพิ่ม</label>
                <input type="txt" name="create_date" class="form-control" id="create_date" value="" disabled>
                </div>
                <div class="form-group">
                <label for="update_date">วันที่แก้ไข</label>
                <input type="txt" name="update_date" class="form-control" id="update_date" value="" disabled>
                </div>

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger">ปิด</button>
        </div>
        </form>
      </div>
    </div>
  </div>
