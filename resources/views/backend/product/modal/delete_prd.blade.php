<!-- Modal -->
<div class="modal fade" id="modal_delete_cate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
      <form action="{{url('/admin/product-type/delete')}}" method="POST">
        <div class="modal-header">
          <h5 class="modal-title" >แจ้งเตือน</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
                <input type="hidden" name="hdid_delete" id="hdid_delete">
                @csrf
                คุณต้องการลบข้อมูลใช่หรือไม่

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary">ยกเลิก</button>
          <button type="submit" class="btn btn-primary">ลบ</button>
        </div>
        </form>
      </div>
    </div>
  </div>
