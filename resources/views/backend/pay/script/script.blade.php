<script>
    let ma_pay = $('#manage_pay')
    let ma_edit_pay = $('.btn_edit_pay')
    let ma_delete_pay = $('.btn_delete_pay')


    ma_pay.click(function(){
        $('#title_modal').html('เพิ่มประเภทสินค้า')
        $('#name').val('')
        $('#hdid_edit').val('')
        $('#status').prop('checked', false);
        $('#modal_manage').modal('show')
    })

    ma_edit_pay.click(function(){

        var name = $(this).attr('data-name') //attr = attribute
        var id = $(this).attr('data-id')
        var status = $(this).attr('data-status')

        $('#title_modal').html('แก้ไขประเภทสินค้า')
        $('#modal_manage').modal('show')
        $('#name').val(name)
        $('#hdid_edit').val(id)

        if(status == 0) {
            $('#status').prop('checked', true);
            $('#status').val('')

        }else{
            $('#status').prop('checked', false);
            $('#status').val('1')
        }

    })

        ma_delete_pay.click(function(){

        let id = $(this).attr('data-id')
        $('#hdid_delete').val(id)
        $('#modal_delete_pay').modal('show')


    })

</script>
