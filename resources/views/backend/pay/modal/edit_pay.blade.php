<!-- Modal -->
<div class="modal fade" id="modal_manage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
      <form action="{{url('/admin/pay/save')}}" method="POST">
        <div class="modal-header">
          <h5 class="modal-title" id="title_modal">เพิ่มรายการสินค้า</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
                <input type="hidden" name="hdid_edit" id="hdid_edit">
                @csrf
                <div class="form-group">
                <label for="cate">ชื่อประเภทสินค้า</label>
                <input type="txt" name="name" class="form-control" id="name" aria-describedby="emailHelp" value="">
                </div>
                <div class="form-group form-check">
                <input type="checkbox" name="status" id="status" class="form-check-input" value="0">
                <label class="form-check-label" for="exampleCheck1">ปิดการใช้งาน</label>
                </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary">ยกเลิก</button>
          <button type="submit" class="btn btn-success">บันทึก</button>
        </div>
        </form>
      </div>
    </div>
  </div>
