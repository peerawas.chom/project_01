@extends('Layouts.backend')
@section('contents')
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>ค่าใช้จ่าย</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">หน้าหลัก</a></li>
              <li class="breadcrumb-item active">ค่าใช้จ่าย</li>
            </ol>
          </div>
        </div>
        <div class="row">
            <div class="col-sm-10"></div>
            <div class="col-sm-2">
                <button id="manage_pay" class="btn btn-block btn-success">เพิ่ม</button>
            </div>
        </div>

      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">ค่าใช้จ่าย</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 1%">
                          #
                      </th>
                      <th style="width: 20%">
                          ชื่อค่าใช้จ่าย
                      </th>
                      </th>
                      <th>
                          จำนวนสินค้า
                      </th>
                      <th style="width: 8%" class="text-center">
                          สถานะ
                      </th>
                      <th style="width: 20%">
                      </th>
                  </tr>
              </thead>
              <tbody>
                  <?php
                    $count=0;


                  ?>
                  @foreach ($data as $datas)
                    <?php
                    if($datas->status ==0)
                    {
                        $classname="danger";
                        $text_status = "ปิดการใช้งาน";
                        $process = "gray";
                    }else
                    {
                        $classname="success";
                        $text_status = "เปิดการใช้งาน";
                        $process = "green";
                    }
                    ?>
                  <tr>
                      <td>
                          {{$count+=1}}
                      </td>
                      <td>
                          <a>
                            {{$datas->name}}
                          </a>
                          <br/>
                          <small>
                                Created {{$datas->create_date}}
                        </small>
                      </td>
                      <td class="project_progress">
                          <div class="progress progress-sm">
                              <div class="progress-bar bg-{{$process}} role="progressbar" aria-volumenow="75" aria-volumemin="0" aria-volumemax="100" style="width: 75%">
                              </div>
                          </div>
                          <small>
                              57% Complete
                          </small>
                      </td>
                      <td class="project-state">
                          <span class="badge badge-{{$classname}}">{{$text_status}}</span>
                      </td>
                      <td class="project-actions text-right">
                          <a class="btn btn-info btn-sm btn_edit_pay" data-id="{{$datas->id}}" data-name="{{$datas->name}}" data-status="{{$datas->status}}" href="#">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a class="btn btn-danger btn-sm btn_delete_pay" data-id="{{$datas->id}}" href="#">
                              <i class="fas fa-trash">
                              </i>
                              Delete
                          </a>
                      </td>
                  </tr>

                  @endforeach
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->


    </section>
    <!-- /.content -->
  </div>

    @include('backend.pay.modal.edit_pay')
    @include('backend.pay.modal.delete_pay')
    @include('backend.pay.script.script')
  <!-- /.content-wrapper -->
@endsection
