<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Newuser;
use Symfony\Component\HttpFoundation\Session\Session;

class BackendController extends Controller
{
    public function index() {

        return view('backend.login');
    }

    public function login(Request $request){
        $username = $request->username;
        $password = $request->password;

        $query = Newuser::where('username','=',$username)->where('password','=',$password)->get();
        $total = count($query);
        if($total == 1){
            $name = Session()->put('name',$query[0]->name);

            return redirect('/admin/dashboard');
        }else{
            return redirect('/admin');
        }
    }
}
