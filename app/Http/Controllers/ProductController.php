<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cate_Product;
use App\Product;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function category()
    {
        $data = Cate_Product::orderBy('name','asc')->get();

        return view('backend.product.product_type',[
            'data'=> $data
        ]);
    }

    public function save(Request $request)
    {
        $cate_name = $request->cate;
        $status = 1;
        if(isset($request->status)){
            $status = 0;
        }

        if(isset($request->hdid_edit)){
            $add_cate = Cate_Product::find($request->hdid_edit);
        }else{
            $add_cate = new Cate_Product();
            $add_cate->create_date = now();
        }

        $add_cate->name = $cate_name;
        $add_cate->status = $status;
        $add_cate->update_date = now();
        $add_cate->save();

        return back();
    }

    public function delete(Request $request)
    {
        $id=$request->hdid_delete;
        $delete_cate = Cate_Product::find($id);
        $delete_cate->delete();

        return back();
    }

    public function view(Request $request)
    {

        $id=$request->hdid_view;
        $view_cate = Cate_Product::find($id);

        return back();
    }

    public function store_product(Request $request){
        $store_product = new Product();
        $store_product->prd_name = $request->prd_name;
        $store_product->type_id = $request->type_id;
        $store_product->description = $request->description;
        $store_product->status = $request->status;
        $store_product->stock = $request->stock;
        $store_product->create_date = now();
        $store_product->update_date = now();

        if($request->hasFile('image')) {
            $newfilename = Str::random(10) . '.' . $request->image->extension();
            $request->image->storeAs('product',$newfilename,'public');
            $store_product->image = $newfilename;
        }
        $store_product->save();
        return redirect('/admin/product');
    }

    public function add(Request $request){
        $data = Cate_Product::where('status',1)->get();
        return view('backend.product.product_add',[
            'data'=> $data
        ]);
    }

    public function product(Request $request){
        $pic = Product::orderBy('id','asc')->get();
        return view('backend.product.product',[
            'pic'=> $pic
        ]);
    }
}
