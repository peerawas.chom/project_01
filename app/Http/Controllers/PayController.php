<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Type_Pay;

class PayController extends Controller
{
    public function pay()
    {
        $data = Type_Pay::orderBy('name','asc')->get();

        return view('backend.pay.pay',[
            'data'=> $data
        ]);
    }

    public function save(Request $request)
    {
        $pay_name = $request->name;
        $status = 1;
        if(isset($request->status)){
            $status = 0;
        }

        if(isset($request->hdid_edit)){
            $add_pay = Type_Pay::find($request->hdid_edit);
            $add_pay->create_date = now();
        }else{
            $add_pay = new Type_Pay();
            $add_pay->create_date = now();
        }

        $add_pay->name = $pay_name;
        $add_pay->status = $status;
        $add_pay->update_date = now();
        $add_pay->save();

        return back();
    }

    public function delete(Request $request)
    {
        $id=$request->hdid_delete;
        $delete_pay = Type_Pay::find($id);
        $delete_pay->delete();

        return back();
    }

    public function view(Request $request)
    {

        $id=$request->hdid_view;
        $view_pay = Type_Pay::find($id);

        return back();
    }
}
