<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Newuser extends Model
{
    protected $table="user";
    protected $primarykey = "id";
    public $timestamps = false;
}
