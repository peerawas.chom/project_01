<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cate_Product extends Model
{
    protected $table = "case";
    protected $primarykey = "id";
    public $timestamps = false;
}
