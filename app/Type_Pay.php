<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type_Pay extends Model
{
    protected $table ="type_pay";
    protected $primarykey ="id";
    public $timestamps = false;
}
